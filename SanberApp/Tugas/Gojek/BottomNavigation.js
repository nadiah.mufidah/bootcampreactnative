import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, ScrollView } from 'react-native';

export default class BottomNavigation extends Component {
    render () {
        return (
            <View style={{flex: 1}}>  
                <View style={{backgroundColor: 'white', flex: 1}}>
                <View style={{marginHorizontal: 17, flexDirection: 'row', paddingTop: 15}} >
                <View style={{position:'relative', flex: 1}}>
                    <TextInput placeholder="What do you want to eat?" style={{borderWidth: 1, borderColor: '#E8E8E8', borderRadius: 25, height: 40, fontSize: 13, paddingLeft: 45, paddingRight: 20, backgroundColor: 'white', marginRight: 18 }}/>
                    <Image style={{height:26, width:26, position:'absolute', top: 5, left: 12}} source={require('./icon/search.png')} />
                </View>
                <View style={{width: 35, alignItems:'center', justifyContent: 'center'}}>
                    <Image style={{height:26, width:26}} source={require('./icon/promo.png')}/>
                </View>
                </View>
                {/* <Gopay /> */}
                <View style={{marginHorizontal: 17, marginTop: 8}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: '#2C5FB8', borderTopLeftRadius: 6, borderTopRightRadius: 6, padding: 14 }}>
                        <Image style={{height:25, width:90}} source={require('./icon/gopay.png')}/>
                        <Text style={{color: 'white', fontSize: 17, fontWeight: 'bold'}}>Rp 50.000</Text>
                    </View>
                    <View style={{flexDirection: 'row', paddingTop: 20, paddingBottom: 14, backgroundColor: '#2F65BD', borderBottomLeftRadius: 6, borderBottomRightRadius: 6}}>
                        <View style={{backgroundColor: '#2F65BD', flex: 1, alignItems:'center', justifyContent:'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/pay.png')}/>
                            <Text style={{fontSize: 13, fontWeight: 'bold', color: 'white', marginTop: 15}}>Pay</Text>
                        </View>
                        <View style={{backgroundColor: '#2F65BD', flex: 1, alignItems:'center', justifyContent:'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/nearby.png')}/>
                            <Text style={{fontSize: 13, fontWeight: 'bold', color: 'white', marginTop: 15}}>Nearby</Text>
                        </View>
                        <View style={{backgroundColor: '#2F65BD', flex: 1, alignItems:'center', justifyContent:'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/topup.png')}/>
                            <Text style={{fontSize: 13, fontWeight: 'bold', color: 'white', marginTop: 15}}>Top Up</Text>
                        </View>
                        <View style={{backgroundColor: '#2F65BD', flex: 1, alignItems:'center', justifyContent:'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/more.png')}/>
                            <Text style={{fontSize: 13, fontWeight: 'bold', color: 'white', marginTop: 15}}>More</Text>
                        </View>
                    </View>
                </View>
                {/* <MainFeature /> */}
                <View style={{flexDirection: 'row', flexWrap: 'wrap',  marginTop: 18}}>
                    <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', marginBottom: 18}}>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-ride.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO RIDE</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-car.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO CAR</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-bluebird.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO BLUEBIRD</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-send.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO SEND</Text>
                        </View>
                    </View>
                    <View style={{justifyContent: 'space-between', flexDirection: 'row', width: '100%', marginBottom: 18}}>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-deals.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO DEALS</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-pulsa.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO PULSA</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/go-food.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>GO FOOD</Text>
                        </View>
                        <View style={{width: '25%', alignItems: 'center'}}>
                            <View style={{width: 58, height: 58, borderWidth: 1, borderColor: 'pink', borderRadius: 18, alignItems: 'center', justifyContent: 'center'}}>
                                <Image style={{height:40, width:40}} source={require('./icon/more.png')}/>
                            </View>
                            <Text style={{fontSize: 11, fontWeight: 'bold', textAlign: 'center', marginTop: 6}}>MORE</Text>
                        </View>
                    </View>
                </View>
                <View style={{height: 17, backgroundColor: '#F2F2F4', marginTop: 20}} ></View>
                {/* News Section */}
                <View  style={{paddingTop: 16, paddingHorizontal: 16 }}>
                    <View>
                        <Image source={require('./dummy/sepak-bola.jpg')} style={{height: 350, width:'100%', borderRadius: 10}}/>
                    </View>
                    <View style={{paddingTop: 16, paddingBottom: 20}}>
                        <Text style={{fontSize: 16, fontWeight: 'bold'}}>GO NEWS</Text>
                        <Text style={{fontSize: 14, fontWeight: 'normal'}}>Dimas Drajat selamatkan penalti, Timnas U-23 kalahkan Brunei</Text>
                    </View>
                </View>
                </View>
                <View style={{height: 55, flexDirection: 'row', justifyContent: 'center'}}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/home-active.png')}></Image>
                        <Text style={{fontSize: 10, color: '#545454', marginTop: 4, color: '#43AB4A'}}>Home</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image style={{height:26, width:26}} source={require('./icon/order.png')} />
                        <Text style={{fontSize: 10, color: '#545454', marginTop: 4}}>Order</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>                     
                        <Image style={{height:26, width:26}} source={require('./icon/help.png')} />                         
                        <Text style={{fontSize: 10, color: '#545454', marginTop: 4}}>Help</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>                       
                        <Image style={{height:26, width:26}} source={require('./icon/inbox.png')} />                     
                        <Text style={{fontSize: 10, color: '#545454', marginTop: 4}}>Inbox</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>                       
                        <Image style={{height:26, width:26}} source={require('./icon/account.png')} />                       
                        <Text style={{fontSize: 10, color: '#545454', marginTop: 4}}>Account</Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    backgroundColor: 'pink',
    flex: 1
})
