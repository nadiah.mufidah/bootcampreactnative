import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

import AboutScreen from '../Pages/AboutScreen';
import AddScreen from '../Pages/AddScreen';
import Homescreen from '../Pages/Home';
import LoginScreen from '../Pages/Login';
import ProjectScreen from '../Pages/ProjectScreen';
import Setting from '../Pages/Setting';
import SkillProject from '../Pages/SkillProject';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

// const styles = StyleSheet.create({})
const navigation=()=>{
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="HomeScreen" component={Homescreen} />
                <Stack.Screen name="MainApp" component={MainApp} />
                <Stack.Screen name="Drawwer" component={Drawwer} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

const MainApp =()=>{
    return (
        <Tab.Navigator>
            <Tab.Screen name="AboutScreen" component={AboutScreen} />
            <Tab.Screen name="AddScreen" component={AddScreen} />
            <Tab.Screen name="SkillProject" component={SkillProject} />
        </Tab.Navigator>
    )
}

const Drawwer=()=>{
    return(
        <Drawer.Navigator>
            <Drawer.Screen name="App" component={MainApp} />
            <Drawer.Screen name="AboutScreen" component={AboutScreen} />
        </Drawer.Navigator>
    )
}

export default navigation;