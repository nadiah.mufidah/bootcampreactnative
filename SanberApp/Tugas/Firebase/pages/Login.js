import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Login({navigation}) {
     
    const firebaseConfig={
        apiKey: "AIzaSyAo_BC7aBwsRdvd5oUwj0lZXaK-XnRPZx8",
        authDomain: "authcontext-7d0e1.firebaseapp.com",
        projectId: "authcontext-7d0e1",
        storageBucket: "authcontext-7d0e1.appspot.com",
        messagingSenderId: "870670194922",
        appId: "1:870670194922:web:1b219d97b4360c7566df93"
     };
     if(!firebase.apps.length){
         firebase.initializaApp(firebaseConfig)
     }
      const [email, setEmail] = useState("");
      const [password, setPassword] = useState("");

      const submit=()=>{
          const data = {
              email,
              password
          }
          console.log(data)
          firebase.auth().signInWithEmailAndPassword(email, password).then(()=>{
              console.log("berhasil login");
              navigation.navigate("Home")
          }).catch(()=>{
              console.log("Login gagal")
          })
      }
    return (
        <View style={styles.container}>
            <Text>Login</Text>
            <TextInput 
                style={styles.input}
                placeholder="Masukin Email"
                value={email}
                onChangeText={(value)=>setEmail(value)}

            />
             <TextInput 
                style={styles.input}
                placeholder="Masukin Password"
                value={password}
                onChangeText={(value)=>setPassword(value)}
            />
            <Button onPress={submit} title="Login"/>
            <TouchableOpacity style={{marginTop: 30}}
            // onPress={()=>navigation.navigate("Register")}
            onPress={()=>alert("hello world")}
            >
                <Text>Buat Akun</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    input:{
        borderWidth:1,
        borderColor:'grey',
        paddingHorizontal:10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
