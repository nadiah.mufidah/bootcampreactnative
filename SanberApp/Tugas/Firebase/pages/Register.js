import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Register({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const firebaseConfig={
            apiKey: "AIzaSyAQFVWwqQbWMl6k4hi5df5fpFLdBT2lWmA",
            authDomain: "authenticationfirebasern-a52f5.firebaseapp.com",
            projectId: "authenticationfirebasern-a52f5",
            storageBucket: "authenticationfirebasern-a52f5.appspot.com",
            messagingSenderId: "697092832274",
            appId: "1:697092832274:web:f4650ec49a644624c02ccb"
        };
    if(!firebase.apps.length){
        firebase.initializaApp(firebaseConfig)
     }

    const submit=()=>{
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email, password).then(()=>{
        // console.log('Register Berhasil');
            navigation.navigate("Home");
        }).catch(()=>{
        console.log("Register Gagal")
        })
      }
    return (
        <View style={styles.container}>
            <Text>Register</Text>
            <TextInput
                style={styles.input}
                placeholder="Masukin Email"
                value={email}
                onChangeText={(value)=>setEmail(value)}

            />
             <TextInput
                style={styles.input}
                placeholder="Masukin Password"
                value={password}
                onChangeText={(value)=>setPassword(value)}
            />
            <Button onPress={submit} title="REGISTER"/>
            <TouchableOpacity style={{marginTop: 30}}
            // onPress={()=>navigation.navigate("Register")}
            onPress={()=>alert("hello world")}
            >
                <Text>Buat Akun</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center'
    },
    input:{
        borderWidth:1,
        borderColor:'grey',
        paddingHorizontal:10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
