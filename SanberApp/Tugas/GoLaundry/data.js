const Data =[
    {
        id : "1",
        harga: 15000,
        image: require("./assets/kiloan.png"),
        title : "Laundry Kiloan",
        desc : "Laundry ini melayani transaksi dalam jumlah pakaian per kg",
        type :"Paketan harga laundry dalam jumlah per kg"
    },
    {
        id : "2",
        harga: 20000,
        image: require("./assets/satuan.png"),
        title : "Laundry Satuan",
        desc : "Laundry ini melayani transaksi hanya 1 pakaian",
        type :"Paketan harga laundry dalam jumlah satuan/pcs"
    },
    {
        id : "3",
        harga: 50000,
        image: require("./assets/dress.png"),
        title : "Laundry VIP",
        desc : "Laundry ini melayani transaksi pakaian dengan pencucian khusus",
        type :"Paketan harga laundry untuk pakaian khusus"
    },
    {
        id : "4",
        harga: 50000,
        image: require("./assets/karpet.png"),
        title : "Laundry Karpet",
        desc : "Laundry ini melayani pencucian karpet",
        type :"Paketan harga laundry untuk karpet"
    },
    {
        id : "5",
        harga: 15000,
        image: require("./assets/setrika.png"),
        title : "Hanya Setrika",
        desc : "Laundry ini hanya melayani transaksi setrika tanpa cuci",
        type :"Paketan harga laundry untuk sentrika saja"
    },
    {
        id : "6",
        harga: 40000,
        image: require("./assets/express.png"),
        title : "Laundry Express",
        desc : "Laundry ini melayani transaksi dapat diambil dalam 24 jam",
        type :"Paketan laundry untuk pelayanan cepat"
    },
    {
        id : "7",
        harga: 80000,
        image: require("./assets/sepatu.png"),
        title : "Laundry Sepatu",
        desc : "Laundry ini melayani pencucian sepatu",
        type :"Paketan laundry untuk sepasang sepatu"
    },
    {
        id : "8",
        harga: 100000,
        image: require("./assets/helmet.png"),
        title : "Laundry Helm",
        desc : "Laundry ini melayani pencucian helm",
        type :"Paketan laundry untuk helm"
    }
]

export {Data};