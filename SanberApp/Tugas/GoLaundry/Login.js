import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TextInput, Button } from "react-native";
import Home from "./Home";

export default function Login({ navigation }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const submit = () => {
    console.log({ username, password });
    if (password == '12345678'){
      setIsError(false);
    navigation.navigate ('Home', {username});
    } else {
      setIsError(true);
    }
  };
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: 50, fontWeight: "bold", paddingBottom: 10 }}>Go Laundry</Text>
      <Image
        style={{ height: 200, width: 200 }}
        source={require("./assets/logo.jpg")}
      />
      <View>
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Username"
          value={username}
          onChangeText={(value)=>setUsername(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Masukan Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <TextInput
          style={{
            borderWidth: 1,
            paddingVertical: 10,
            borderRadius: 5,
            width: 300,
            marginBottom: 10,
            paddingHorizontal: 10,
          }}
          placeholder="Confirmed Password"
          value={password}
          onChangeText={(value)=>setPassword(value)}
        />
        <Button onPress={submit} title="Login" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },
});
