import React, { useState} from 'react';
import { Button, FlatList, Image, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View, } from 'react-native';
import {Data} from './data'

export default function Home({route, navigation}) {
    const { username } = route.params;
    const [totalPrice, setTotalPrice] = useState(0);

    const currencyFormat=(num)=> {
        return `Rp ${num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
    };

    const updateHarga =(price)=>{
        console.log("UpdatePrice : " + price);
        const temp = Number(price) + totalPrice;
        console.log(temp)
        setTotalPrice(temp)
     
    }
    return (
        <View style={styles.container}>
            <View style={{flexDirection:'row', justifyContent:"space-between", padding: 16}}>
                <View>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Selamat Datang,</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{username}</Text>
                </View>
                <View>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Total Harga:</Text>
                    <Text style={{fontSize:18, fontWeight:'bold'}}> {currencyFormat(totalPrice)}</Text>
                </View>
            </View>
            <View style={{alignItems:'center',  marginBottom: 20, paddingBottom: 60}}>
               <FlatList
               numColumns={4}
               data={Data}
               renderItem={({item}) => (
               <View style={styles.productCard}>
                   <Text style= {styles.productTitle}>{item.title}</Text>
                   <Image style={styles.productImage} source={item.image} />
                   <Text style={styles.productPrice}>{item.price}</Text>
                   <Text style={styles.productDesc}>{item.type}</Text>
                   <Button title = 'Pilih' onPress={() => {updateHarga(item.harga)}} />
               </View>
               )}
               />
            </View>
            <TouchableOpacity style={{height: 55, flexDirection: 'row', justifyContent: 'center', bacgroundColor:'white', flex: 1}}>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image style={{height:30, width:30}} source={require('./assets/iconHomeActive.jpg')}></Image>
                        <Text style={{fontSize: 17, color: '#545454', marginTop: 4}}>Home</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <Image style={{height:26, width:26}} source={require('./assets/iconPesananActive.jpg')}/>
                        <Text style={{fontSize: 17, color: '#545454', marginTop: 4}}>Pesanan</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center'}}>                       
                        <Image style={{height:26, width:26}}  source={require('./assets/iconAkunActive.jpg')}/>                       
                        <Text style={{fontSize: 17, color: '#545454', marginTop: 4}}>Akun</Text>
                    </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,        
        backgroundColor:'#C1F2DC', 
    },  
    productCard :{
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 1,
        borderRadius: 8,
        borderColor: 'grey',
        backgroundColor: 'white',
        margin: 5,
        padding: 10,
    },
    productImage :{
        height: 200,
        width: 200,
    },
    content:{
        width: 150,
        height: 220,        
        margin: 5,
        borderWidth:1,
        alignItems:'center',
        borderRadius: 5,
        borderColor:'grey',    
    },
        
})
