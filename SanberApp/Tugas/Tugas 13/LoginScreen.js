// import React from "react";
// import { Image, StyleSheet, Text, TextInput, View } from "react-native";
// import { MaterialComunityIcons } from "@expo/vector-icons";
// import colors from "./Config/colors";
// import typhography from "./Config/typhography";
// // import { Colors } from "react-native/Libraries/NewAppScreen";

// export default function LoginScreen (){
//     return (
//         <View>
//             <Image
//                 style={{
//                     height: 116,
//                     width: 375,
//                 }}
//                 source={require("./asset/logo.png")}
//             />
//             <View styles={styles.form}>
//                 <Text style={typhography.h3}>Masuk</Text>
//                 <View style={styles.formFieldContainer}>
//                      <View style={styles.formTextInputBorder}>
//                          <TextInput 
//                             style={(typhography.sub1, { flex: 1})}
//                             placeholder="Username/Email"
//                         />
//                         <MaterialComunityIcons
//                             name="eye"
//                             size={24}
//                             color={Colors.srufaceMedium}
//                         />                         
//                 </View>
//                 <Text
//                 style={(typhography.sub1, { color: colors.error,
//                         paddingLeft: 11 })}
//                 >
//                     This is Error Message
//                 </Text>
//                 </View>
//             </View>
//         </View>
//     )
// }

// const styles = StyleSheet.create({
//     formTextInputBorder: {
//         borderRadius: 5,
//         borderWidth: 1,
//         flexDirection: "row",
//         height: 54,
//         justifyContent: "center",
//         padding: 10,
//     },
//     formTextInput: {
//         fontSize: 16,
//     },
// });

import React from 'react'
import { Platform, 
        Image, 
        ScrollView, 
        StyleSheet, 
        Text, 
        View, 
        TextInput, 
        TouchableOpacity, 
        Button, 
        KeyboardAvoidingView } from 'react-native'


const LoginScreen = ()=>{
    return (
        <KeyboardAvoidingView 
        behavior = {Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <ScrollView>
            < View styles={styles.containerView}>
                <Image source={require("./asset/logo.png")}/>
                <Text style={styles.logintext}>Login</Text>
                <View styles={styles.formInput}>
                    <Text style={styles.formtext}>Username</Text>
                   <TextInput style={styles.input} />
                </View>
                <View style={styles.formInput}>
                    <Text style={styles.formtext}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true} />
                 </View>
                 <View style={styles.kotaklogin} >
                     <TouchableOpacity style={styles.btlogin}>
                         <Text style={styles.textbt}>Masuk</Text>
                     </TouchableOpacity>
                     <Text style={styles.autotext}>Atau</Text>
                     <TouchableOpacity styles={styles.btreg}>
                         <Text style={styles.textbt}>Daftar</Text>
                     </TouchableOpacity>
                 </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container:{
        flex: 1,
    }, logintext: {
        fontSize: 24,
        marginTop: 53,
        textAlign: 'center',
        color : '#003366',
        marginVertical: 20
    },
    formtext: {
        color: '#003366'
    }, autotext: {
        fontSize: 20,
        color: '#3EC6FF'
    },
    formInput:{
        marginHorizontal: 30,
        marginVertical: 5,
        alignContent: 'center',
        width: 294
    },
    input: {
        height: 40,
        borderColor: '#003366',
        padding: 10,
        borderWidth: 1
    }, btlogin:{
        alignItems: 'center',
        backgroundColor: '#3EC6FF',
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140,
    }
})
