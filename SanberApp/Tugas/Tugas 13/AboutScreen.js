import React from 'react'
import { StyleSheet, 
        Text, 
        View,
        ScrollView,
        Image,
        TextInput,
        Button
        } from 'react-native'

const AboutScreen = ()=>{
    return(
        <ScrollView>
        <View style={styles.container}>
            <Text styles={styles.judul}>Tentang Saya</Text>
            <FontAwesome5 
                name="user-circle"
                size={200}
                solidcolor="#EFEFEF"
                style={styles.icon} 
            />
            <Text style={styles.judul}>Nadiah Mufidah</Text>
            <Text style={styles.kerjaan}> Tukang Ketik</Text>
            <View style={styles.kotak}>
                <Text style={styles.juduldalam}>Portofolio</Text>
                <View style={styles.kotakdalam}>
                    <View>
                        <FontAwesome5 
                        name="gitlatb"
                        color='#003366'
                        size={40}
                        style={styles.icon}
                        />
                    </View>
                    <View style={styles.textName}></View>
                        <Text style={styles.textdalam}>@nadmuf</Text>
                    <View>
                        <FontAwesome5 
                        name="gitlatb"
                        size={40}
                        color="#003366"
                        style={styles.icon}
                        />
                        <Text style={styles.textdalam}>@nadmuf</Text>
                    </View>
                </View>
            </View>
            <View style={styles.kotak}>
                <Text style={styles.juduldalam}>Hubungi saya</Text>
                <View style={styles.kotakdalamver}>
                    <View style={styles.kotakdalamverhub}>
                        <View>
                            <FontAwesome5 name="facebook" size={40} color= '#003366'
                        style={styles.icon}/>
                        </View>
                        <View style={styles.textName}>
                            <Text style={styles.textdalam}>@nadmuf</Text>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                             <View>
                                 <FontAwesome5 name="instagram" size={40} color= '#003366'
                        style={styles.icon}/>       
                            </View>
                            <View style={styles.textName}>
                                <Text style={styles.textdalam}>@nadmuf</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                        <View>
                            <FontAwesome5 name="twitter" size={40} color= '#003366'
                        style={styles.icon}/>
                        </View>
                        </View>
                            <Text style={styles.textdalam}>@nadmuf</Text>
                        </View>
                </View>
            </View>
        </View>
    </ScrollView>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        marginTop:64,
    },
    judul: {
        fontSize: 36,
        fontWeight: "bold",
        color: '#003366',
        textAlign: 'center',
    }, icon: {
        textAlign: 'center',
    }, name:{
        fontSize: 24,
        fontWeight: "bold",
        color:'#003366',
        textAlign: 'center'
    }, kerjaan:{
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF',
        textAlign: 'center',
        marginBottom: 7
    }, kotak: {
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#EFEFEF',
        marginBottom: 9
    }, kotakdalam: {
        borderTopWidth: 2,
        borderTopColor:'#003366',
        flexDirection: 'column',
        justifyContent: 'space-around'
    }, kotakdalamver: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around',
    }, kotakdalamverhub: {
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 2,
    }, juduldalam: {
        fontSize: 18,
        color: '#003366'
    }, textdalam: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: 'center'
    }, input: {
         height: 40,
         borderColor: 'grey',
         borderWidth: 1,
    }, textName:{
        justifyContent: 'center',
        marginLeft: 10
    }
})

