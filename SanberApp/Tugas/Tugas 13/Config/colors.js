import { StyleSheet } from "react-native";
import colors from "./colors";

const typography = StyleSheet.create({
  button: {
    fontSize: 14,
    fontWeight: "700",
  },
  body2: {
    fontSize: 14,
    color: colors.srufaceMedium,
  },
  h1: {
    fontSize: 96,
    fontWeight: "100",
  },
  h3: {
    fontSize: 48,
    color: colors.surfaceHigh,
  },
  h4: {
    fontSize: 34,
    color: colors.surfaceHigh,
  },
  sub1: {
    fontSize: 16,
    color: colors.srufaceMedium,
  },
});

export default typography;