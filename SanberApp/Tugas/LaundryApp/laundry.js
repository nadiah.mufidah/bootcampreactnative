import React from 'react'
import { StyleSheet,Text, View } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
 

const laundry = () => {
    return (
        <NavigationContainer>  
            <Router />
        </NavigationContainer>
    )
}

export default laundry

const styles = StyleSheet.create({})