import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'

const Akun = () => {
    return (
        <View>
            <View style={{justifyContent: 'left', alignItems: 'left'}}>
                <Image style={{height: 100, width: 100}} source={require('../asset/golaundry.png')} />
            </View>
            <View style={{justifyContent: 'center', alignItems: 'center'}}>
                 <Text style={{fontSize: 24, fontWeight: 'bold'}}>Akun Saya</Text>
            </View>
            <TouchableOpacity>
                <Text>Halo!</Text>
            </TouchableOpacity>
        </View>
    )
}

export default Akun

const styles = StyleSheet.create({})
