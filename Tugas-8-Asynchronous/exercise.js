// // Deklarasi function yang memilik callback sebagai parameter
// function periksaDokter(nomerAntri, callback) {
//     if(nomerAntri > 50 ) {
//         callback(false)
//     } else if(nomerAntri < 10) {
//         callback(true)
//     }    
// } 
// // Menjalankan function periksaDokter yang sebelumnya sudah dideklarasi
// periksaDokter(65, function(check) {
//     if(check) {
//         console.log("sebentar lagi giliran saya")
//     } else {
//         console.log("saya jalan-jalan dulu")
//     }
// }) 

// function askMom() {
//     willIGetNewPhone
//         .then(function (fulfilled) {
//             // yay, you got a new phone
//             console.log(fulfilled);
//          // output: { brand: 'Samsung', color: 'black' }
//         })
//         .catch(function (error) {
//             // oops, mom don't buy it
//             console.log(error.message);
//          // output: 'mom is not happy'
//         });
// }
 
// // Tanya Mom untuk menagih janji
// askMom() 

setTimeout(() => {
    console.log("Saya membeli kacamata");
    setTimeout (() => {
        console.log("Saya membeli baju");
        setTimeout (() => {
            console.log("Saya sudah sampai rumah");
        }, 8000);       
    }, 5000);
}, 3000);

