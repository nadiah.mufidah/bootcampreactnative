// di index.js
let readBooks = require("./callback");
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
// readBooks(5000, books[0], (sisaWaktu) => {
//     console.log("Sisa waktu =", sisaWaktu);
// });

// readBooks(5000, books[0], (sisaWaktu) => {
//     readBooks(sisaWaktu, books[1], (sisaWaktu) => {
//         console.log("Sisa waktu = ", sisaWaktu);
//     });
// });

// readBooks(10000, books[0], (sisaWaktu) => {
//     if(books[0].timeSpent < sisaWaktu) {
//         readBooks(sisaWaktu, books[0], (sisaWaktu) => {
//             console.log("Sisa waktu = ", sisaWaktu);
//         });
//     }else console.log ("Saya selesai membaca");
// });

// readBooks(7000, books[1], (sisaWaktu) => {
//     if(books[1].timeSpent < sisaWaktu) {
//         readBooks(sisaWaktu, books[1], (sisaWaktu) => {
//             console.log("Sisa waktu = ", sisaWaktu);
//         });
//     }else console.log ("Saya selesai membaca");
// });

// readBooks(5000, books[2], (sisaWaktu) => {
//     if(books[2].timeSpent < sisaWaktu) {
//         readBooks(sisaWaktu, books[2], (sisaWaktu) => {
//             console.log("Sisa waktu = ", sisaWaktu);
//         });
//     }else console.log ("Saya selesai membaca");
// });
