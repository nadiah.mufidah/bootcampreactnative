let readBooksPromise = require('./promise');
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
// readBooksPromise(10000, books[0])
//     .then((value) => {
//         console.log(value);
//     })
//     .catch((reason) => {
//         console.log(reason);
//     });

// readBooksPromise(7000, books[1])
//     .then((value) => {
//         console.log(value);
//     })
//     .catch((reason) => {
//         console.log(reason);
//     });

readBooksPromise(5000, books[2])
    .then((value) => {
        console.log(value);
    })
    .catch((reason) => {
        console.log(reason);
    });
