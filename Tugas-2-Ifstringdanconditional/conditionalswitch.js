console.log("nomor 2 conditional")
var hari = 25 ;
var bulan = 1 ;
var tahun = 1945 ;
var bulanStr;
switch (bulanStr) {
    case 1: { console.log('Januari'); break; }
    case 2: { console.log('Februari'); break; }
    case 3: { console.log('Maret'); break; }
    case 4: { console.log('April'); break; }
    case 5: { console.log('Mei'); break; }
    case 6: { console.log('Juni'); break; }
    case 7: { console.log('Juli'); break; }
    case 8: { console.log('Agustus'); break; }
    case 9: { console.log('September'); break; }
    case 10: { console.log('Oktober'); break; }
    case 11: { console.log('November'); break; }
    case 12: { console.log('Desember'); break; }
    default: { console.log("Masukan Bulan"); break;}
}
console.log(hari + " " + bulanStr + " " + tahun);

console.log("Jawaban Nomor 2 Conditional Switch")
var hari = 25 ;
var bulan = 1 ;
var tahun = 1945 ;

var bulanNama;

if (hari >= 1 && hari <=31){
    if(bulan >= 1 && bulan <=12){
        if(tahun>=1900 && bulan <= 2200){
            switch(bulan){
                case 1 :
                    bulanNama = "Januari";
                    break;
                case 2 :
                    bulanNama = "Februari";
                    break;
                case 3 :
                    bulanNama = "Maret";
                    break;
                case 4 :
                    bulanNama = "April";
                    break;
                case 5 :
                    bulanNama = "Mei";
                    break;
                case 6 :
                    bulanNama = "Juni";
                    break;
                case 7 :
                    bulanNama = "Juli";
                    break;
                case 8 :
                    bulanNama = "Agustus";
                    break;
                case 9 :
                    bulanNama = "September";
                    break;
                case 10 :
                    bulanNama = "Oktober";
                    break;
                case 11 :
                    bulanNama = "November";
                    break;
                case 12 :
                    bulanNama = "Desember";
                    break;  
                default  :
                    break; 
            }
            console.log(hari + " " + bulanNama + " " + tahun);
        } else{
            console.log("Masukan tahun diantara (1900-2200)");
        }
    } else{
        console.log("Masukan bulan diantara (1-12)");
        }
    } else{
    console.log("Masukan tanggal diantara(1-30)");
}

