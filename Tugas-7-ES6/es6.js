console.log("SOAL NO 1")
const golden = `This is golden!!`
console.log(golden)

console.log("SOAL NO 2")
const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
    }
}
const person = newFunction("William", "Imoh");
console.log(person);

console.log("SOAL NO 3")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)

console.log("SOAL NO 4")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const equator = [...west, ...east];
console.log(equator);

console.log("SOAL NO 5")
const planet = "earth"
const view = "glass"

const before = `Lorem ${planet} dolor sit amet, consectetur adipiscing elit, ${view} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before) 


