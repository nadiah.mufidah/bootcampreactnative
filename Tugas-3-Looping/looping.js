console.log("Soal 1 : Looping Pertama")
var flag = 2;
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
    if (flag %2 == 0)
  console.log(flag + " " + '- I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}

console.log("Soal 1 : Looping Kedua")

var flag = 20;
while(flag >= 2) {
  flag -= 2;
  if (flag == 20){
    console.log("Soal 1 : Looping Kedua");
  }
  console.log(flag + "- I will become a mobile developer");
}


console.log("Soal 2")
for (let i = 1; i <= 20; i++) {
  var ganjil = i % 2 === 0;
  if (ganjil){
    console.log(i + "- Berkualitas");
  } else {
    if (i % 3 === 0) {
      console.log(i + "- I Love Coding ");
      continue;
    }
    console.log(i + "- Santai");
    }
}

console.log("Soal 3")
var tinggi = 7;
var alas = 7;
var buff = "";

for (let i = 0; i < tinggi; i++){
  for (let j = 0; j <= i; j++){
    buff += "#";
  }
  buff += "\n";
}
console.log(buff)

console.log("Soal 4")
var row = 8;
var col = 8;
var buff = "";

for (let i = 0; i < row; i++){
  var ganjil = i % 2 == 0;
  if (ganjil){
    for (let j = 0; j < col/2; j++) {
      buff += " #";
    }
  } else {
    for (let j = 0; j < col/2; j++) {
      buff += "# ";
    }
  }
  buff += "\n";
}
console.log(buff);
